﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OxxoTVZacAPI.Models
{
    public class reverse
    {
        private string _idReverse = "";
        private string _amount = "";
        private string _auth = "";
        private string _account = "";
        private string _code = "";
        private string _errorDesc = "";
        private string _messageTicket = "";

        public string idReverse
        {
            get { return _idReverse; }
            set { _idReverse = value; }
        }

        public string amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

        public string auth
        {
            get { return _auth; }
            set { _auth = value; }
        }

        public string account
        {
            get { return _account; }
            set { _account = value; }
        }

        public string code
        {
            get { return _code; }
            set { _code = value; }
        }

        public string errorDesc
        {
            get { return _errorDesc; }
            set { _errorDesc = value; }
        }

        public string messageTicket
        {
            get { return _messageTicket; }
            set { _messageTicket = value; }
        }

    }
}