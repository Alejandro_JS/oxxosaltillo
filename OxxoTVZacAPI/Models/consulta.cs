﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.ObjectModel;

namespace OxxoTVZacAPI.Models
{
    public class consulta
    {
        private string _account = "";
        private string _name = "";
        private string _address = "";
        private string _status = "";
        private string _reference = "";
        private string _partial = "";
        private ObservableCollection<concept> _concepts;
        private string _code = "";
        private string _message = "";

        public string account
        {
            get { return _account; }
            set { _account = value; }
        }

        public string name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string address
        {
            get { return _address; }
            set { _address = value; }
        }

        public string status
        {
            get { return _status; }
            set { _status = value; }
        }

        public string reference
        {
            get { return _reference; }
            set { _reference = value; }
        }

        public string partial
        {
            get { return _partial; }
            set { _partial = value; }
        }

        public ObservableCollection<concept> concepts
        {
            get 
            {
                if(_concepts != null)
                    return _concepts;
                else
                {
                    _concepts = new ObservableCollection<concept>();
                    //concept c = new concept();
                    //_concepts.Add(c);
                    return _concepts;
                }
            }
            set { _concepts = value; }
        }

        public string code
        {
            get { return _code; }
            set { _code = value; }
        }

        public string message
        {
            get { return _message; }
            set { _message = value; }
        }
    }
}