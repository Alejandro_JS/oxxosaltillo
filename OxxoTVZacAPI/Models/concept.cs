﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OxxoTVZacAPI.Models
{
    public class concept
    {
        private string _description;
        private string _amount;
        private string _operation;

        public string description
        {
            get { return _description; }
            set { _description = value; }
        }

        public string amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

        public string operation
        {
            get { return _operation; }
            set { _operation = value; }
        }
    }
}