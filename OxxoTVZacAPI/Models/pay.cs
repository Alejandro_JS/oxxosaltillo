﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OxxoTVZacAPI.Models
{
    public class pay
    {
        private string _auth = "";
        private string _amount = "";
        private string _messageTicket = "";
        private string _account = "";
        private string _code = "";
        private string _errorDesc = "";

        public string auth
        {
            get { return _auth; }
            set { _auth = value; }
        }

        public string amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

        public string messageTicket
        {
            get { return _messageTicket; }
            set { _messageTicket = value; }
        }

        public string account
        {
            get { return _account; }
            set { _account = value; }
        }

        public string code
        {
            get { return _code; }
            set { _code = value; }
        }

        public string errorDesc
        {
            get { return _errorDesc; }
            set { _errorDesc = value; }
        }
    }
}