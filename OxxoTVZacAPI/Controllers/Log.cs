﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Collections;
using System.Reflection;
using System.Xml.Serialization;
using System.Xml;

namespace OxxoTVZacAPI.Controllers
{
    public class Log
    {
        //private static string _token = "OXXO_dv04142018";
        //private static string _token = "OXXO_qa04142018";
        private static string _token = "OXXO_pr04142018";
        public static void GuardaLog(int contratoCompania, int idCompania, string mensaje, int code, string modulo, string identificadorServidor)
        {
            try
            {
                DBHelper db = new DBHelper(identificadorServidor);
                db.agregarParametro("@contratoCompania", SqlDbType.Int, contratoCompania);
                db.agregarParametro("@idCompania", SqlDbType.Int, idCompania);
                db.agregarParametro("@mensaje", SqlDbType.VarChar, mensaje);
                db.agregarParametro("@codigo", SqlDbType.Int, code);
                db.agregarParametro("@modulo", SqlDbType.VarChar, modulo);
                db.consultaSinRetorno("InsertaLogOxxo");
            }
            catch (Exception ex)
            {

            }
            
        }

        public static void GuardaLog(string account, string mensaje, int code, string modulo, string identificadorServidor)
        {
            try { 
                string[] arr = account.Split('-');
                DBHelper db = new DBHelper(identificadorServidor);
                db.agregarParametro("@contratoCompania", SqlDbType.Int, arr[0]);
                db.agregarParametro("@idCompania", SqlDbType.Int, arr[1]);
                db.agregarParametro("@mensaje", SqlDbType.VarChar, mensaje);
                db.agregarParametro("@codigo", SqlDbType.Int, code);
                db.agregarParametro("@modulo", SqlDbType.VarChar, modulo);
                db.consultaSinRetorno("InsertaLogOxxo");
            }
            catch (Exception ex)
            {

            }
        }

        public static string AgregaCerosIzquierda(string cadena, int longitud)
        {
            while (cadena.Length < longitud)
            {
                cadena = "0" + cadena;
            }
            return cadena;
        }

        public static bool ValidaToken(string token)
        {
            if (_token == token)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static void ActivaContrato(int contratoCompania, int idCompania, string identificadorServidor)
        {
            DBHelper db = new DBHelper(identificadorServidor);
            db.agregarParametro("@contratoCompania", SqlDbType.Int, contratoCompania);
            db.agregarParametro("@idCompania", SqlDbType.Int, idCompania);
            db.consultaSinRetorno("ActivaContratoOxxo");
        }

        public static void LiberaContrato(int contratoCompania, int idCompania, string identificadorServidor)
        {
            DBHelper db = new DBHelper(identificadorServidor);
            db.agregarParametro("@contratoCompania", SqlDbType.Int, contratoCompania);
            db.agregarParametro("@idCompania", SqlDbType.Int, idCompania);
            db.consultaSinRetorno("LiberaContratoOxxo");
        }

        public static string ObtieneValorPorNombre(XmlDocument xmlDocument, string Tag)
        {
            XmlNodeList elemList = xmlDocument.GetElementsByTagName(Tag);
            if (elemList.Count > 0)
            {
                return elemList[0].InnerXml.ToString();
            }
            else
            {
                return "";
            }
        }
    }
}