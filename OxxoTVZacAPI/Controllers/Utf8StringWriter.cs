﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml;
using System.Data;
using OxxoTVZacAPI.Models;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Text;
using System.Xml.Serialization;
using System.IO;

namespace OxxoTVZacAPI.Controllers
{
    public class Utf8StringWriter : StringWriter
    {
        // Use UTF8 encoding but write no BOM to the wire
        public override Encoding Encoding
        {
            get { return new UTF8Encoding(false); } // in real code I'll cache this encoding.
        }
    }
}