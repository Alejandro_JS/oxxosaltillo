﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml;
using System.Data;
using OxxoTVZacAPI.Models;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Text;
using System.Xml.Serialization;
using System.IO;

namespace OxxoTVZacAPI.Controllers
{
    public class consultaController : ApiController
    {
        // GET api/<controller>
        public HttpResponseMessage Get(string client)
        {
            XmlSerializerNamespaces namespaces = new XmlSerializerNamespaces();
            //namespaces.Add(string.Empty, string.Empty);
            namespaces.Add("version", "1.0");
            
            consulta _consulta = new consulta();
            StringWriter stringwriter = new Utf8StringWriter();
            string identificadorServidor = "";
            identificadorServidor = client.Substring(0, 1);
            var serializer = new XmlSerializer(_consulta.GetType());

            if (!identificadorServidor.Equals("1") && !identificadorServidor.Equals("2") && !identificadorServidor.Equals("3") && !identificadorServidor.Equals("4") && !identificadorServidor.Equals("5"))
            {
                _consulta.code = "07";
                _consulta.message = "SERVICIO NO DISPONIBLE";
                _consulta.code = Log.AgregaCerosIzquierda(_consulta.code, 2);
                StringWriter stringwriter2 = new Utf8StringWriter();
                var serializer2 = new XmlSerializer(_consulta.GetType());
                serializer2.Serialize(stringwriter2, _consulta, namespaces);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(stringwriter2.ToString().Replace("consulta", "OLS").Replace("xmlns:", "").Replace("utf", "UTF"), Encoding.UTF8, "application/xml")
                };
            }

            try
            {
                if (client.Length != 11)
                {
                    _consulta.code = "1";
                    _consulta.message = "LONGITUD DE PARAMETRO IMCOMPLETO";
                    Log.GuardaLog(0, 0, _consulta.message, int.Parse(_consulta.code), "consulta", identificadorServidor);
                    _consulta.code = Log.AgregaCerosIzquierda(_consulta.code, 2);
                    serializer.Serialize(stringwriter, _consulta, namespaces);
                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(stringwriter.ToString().Replace("consulta", "OLS").Replace("xmlns:", "").Replace("utf", "UTF"), Encoding.UTF8, "application/xml")
                    };
                }
                else
                {
                    int clvSession;
                    int contratoCompania = 0;
                    int idCompania = 0;
                    contratoCompania = int.Parse(client.Substring(1, 5));
                    idCompania = int.Parse(client.Substring(6, 5));
                    identificadorServidor = client.Substring(0, 1);

                    //Pasa el cobra para checar el adeudo del cliente
                    DBHelper db1 = new DBHelper(identificadorServidor);
                    db1.agregarParametro("@CONTRATO", SqlDbType.Int, contratoCompania);
                    db1.agregarParametro("@CLV_SESSION", SqlDbType.Int, ParameterDirection.Output);
                    db1.agregarParametro("@ERROR", SqlDbType.Int, ParameterDirection.Output);
                    db1.agregarParametro("@MSG", SqlDbType.VarChar, ParameterDirection.Output, 500);
                    db1.agregarParametro("@idCompania", SqlDbType.Int, idCompania);
                    db1.consultaOutput("CobraFS");
                    if (int.Parse(db1.diccionarioOutput["@ERROR"].ToString()) > 0)
                    {
                        _consulta.code = "13";
                        _consulta.message = "REFERENCIA INACTIVA";
                        Log.GuardaLog(int.Parse(client.Substring(0, 5)), int.Parse(client.Substring(5, 5)), db1.diccionarioOutput["@MSG"].ToString(), int.Parse(db1.diccionarioOutput["@ERROR"].ToString()), "consulta", identificadorServidor);
                        _consulta.code = Log.AgregaCerosIzquierda(_consulta.code, 2);
                        serializer.Serialize(stringwriter, _consulta, namespaces);
                        return new HttpResponseMessage()
                        {
                            Content = new StringContent(stringwriter.ToString().Replace("consulta", "OLS").Replace("xmlns:", "").Replace("utf", "UTF"), Encoding.UTF8, "application/xml")
                        };
                    }
                    clvSession = int.Parse(db1.diccionarioOutput["@CLV_SESSION"].ToString());

                    //Obtiene los detalles del cliente
                    DBHelper db2 = new DBHelper(identificadorServidor);
                    db2.agregarParametro("@contratoCompania", SqlDbType.Int, contratoCompania);
                    db2.agregarParametro("@idCompania", SqlDbType.Int, idCompania);
                    SqlDataReader reader = db2.consultaReader("ObtieneDatosClienteOxxo");
                    IEnumerable<consulta> resultadodb2 = db2.MapDataToEntityCollection<consulta>(reader);
                    _consulta = resultadodb2.First();
                    reader.Close();
                    db2.cierraConexion();

                    //Ontiene los detalles de oxxo
                    DBHelper db3 = new DBHelper(identificadorServidor);
                    db3.agregarParametro("@Clv_Session", SqlDbType.Int, clvSession);
                    db3.agregarParametro("@contratoCompania", SqlDbType.Int, contratoCompania);
                    db3.agregarParametro("@idCompania", SqlDbType.Int, idCompania);
                    SqlDataReader reader2 = db3.consultaReader("ObtieneDetalleOxxo");
                    IEnumerable<concept> resultadodb3 = db3.MapDataToEntityCollection<concept>(reader2);
                    _consulta.concepts = new ObservableCollection<concept>(resultadodb3);
                    reader2.Close();
                    db3.cierraConexion();

                    //Vamos a reordenar los detalles
                    IEnumerable<concept> _conceptsP = _consulta.concepts.Where(x => x.operation == "+");
                    IEnumerable<concept> _conceptsN = _consulta.concepts.Where(x => x.operation == "-");
                    IEnumerable<concept> _conceptsT = _consulta.concepts.Where(x => x.operation == "t");
                    ObservableCollection<concept> _conceptsAux = new ObservableCollection<concept>();
                    _conceptsP.ToList().ForEach(_conceptsAux.Add);
                    _conceptsN.ToList().ForEach(_conceptsAux.Add);
                    _conceptsT.ToList().ForEach(_conceptsAux.Add);
                    _consulta.concepts = _conceptsAux;

                    //Agregamog cero a code y convertimos a xml
                    if (_consulta.concepts.Any(x => x.description == "No se puede procesar el pago. Intentelo mas tarde"))
                    {
                        _consulta.concepts.Where(x => x.description == "No se puede procesar el pago. Intentelo mas tarde").FirstOrDefault().description = "Subtotal";
                        _consulta.code = Log.AgregaCerosIzquierda("10", 2);
                        _consulta.message = "REFERENCIA SIN ADEUDO";//PROBLEMA EN CODIGO
                    }
                    else
                    {
                        _consulta.code = Log.AgregaCerosIzquierda(_consulta.code, 2);
                    }

                    //Guardamos log
                    Log.GuardaLog(int.Parse(client.Substring(0, 5)), int.Parse(client.Substring(5, 5)), _consulta.message, int.Parse(_consulta.code), "consulta", identificadorServidor);

                    serializer.Serialize(stringwriter, _consulta, namespaces);
                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(stringwriter.ToString().Replace("consulta", "OLS").Replace("xmlns:", "").Replace("utf", "UTF"), Encoding.UTF8, "application/xml")
                    };
                }
            }
            catch (Exception ex)
            {
                _consulta.code = "07";
                _consulta.message = "SERVICIO NO DISPONIBLE";
                Log.GuardaLog(0, 0, ex.ToString(), int.Parse(_consulta.code), "consulta", identificadorServidor);
                _consulta.code = Log.AgregaCerosIzquierda(_consulta.code, 2);
                StringWriter stringwriter2 = new Utf8StringWriter();
                var serializer2 = new XmlSerializer(_consulta.GetType());
                serializer2.Serialize(stringwriter2, _consulta, namespaces);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(stringwriter2.ToString().Replace("consulta", "OLS").Replace("xmlns:", "").Replace("utf", "UTF"), Encoding.UTF8, "application/xml")
                };
            }
        }

        // GET api/<controller>/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        // POST api/<controller>
        //public void Post([FromBody]string value)
        //{
        //}

        // PUT api/<controller>/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        // DELETE api/<controller>/5
        //public void Delete(int id)
        //{
        //}
    }
}