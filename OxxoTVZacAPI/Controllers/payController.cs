﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml;
using System.Data;
using OxxoTVZacAPI.Models;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Text;
using System.Xml.Serialization;
using System.IO;
namespace OxxoTVZacAPI.Controllers
{
    public class payController : ApiController
    {

        // POST api/<controller>
        public HttpResponseMessage Post(HttpRequestMessage request)
        {
            XmlSerializerNamespaces namespaces = new XmlSerializerNamespaces();
            //namespaces.Add(string.Empty, string.Empty);
            namespaces.Add("version", "1.0");

            pay _pay = new pay();
            StringWriter stringwriter = new Utf8StringWriter();
            var serializer = new XmlSerializer(_pay.GetType());
            string identificadorServidor = "";
            try
            {
                var content = request.Content;
                string xmlContent = content.ReadAsStringAsync().Result;
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.LoadXml(xmlContent);

                XmlNode root = xmlDocument.ChildNodes[1];
                string token = Log.ObtieneValorPorNombre(xmlDocument, "token");//root.ChildNodes[0].InnerText;

                string client = Log.ObtieneValorPorNombre(xmlDocument, "client");//root.ChildNodes[1].InnerText;
                identificadorServidor = client.Substring(0, 1);

                if (!identificadorServidor.Equals("1") && !identificadorServidor.Equals("2") && !identificadorServidor.Equals("3") && !identificadorServidor.Equals("4") && !identificadorServidor.Equals("5"))
                {
                    _pay.code = "07";
                    _pay.errorDesc = "SERVICIO NO DISPONIBLE";
                    _pay.code = Log.AgregaCerosIzquierda(_pay.code, 2);
                    serializer.Serialize(stringwriter, _pay, namespaces);
                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(stringwriter.ToString().Replace("pay", "OLS").Replace("xmlns:", "").Replace("utf", "UTF"), Encoding.UTF8, "application/xml")
                    };
                }

                if (!Log.ValidaToken(token))
                {
                    _pay.errorDesc = "INCONSISTENCIA DE PARAMETROS";
                    _pay.code = "22";
                    _pay.code = Log.AgregaCerosIzquierda(_pay.code, 2);
                    serializer.Serialize(stringwriter, _pay, namespaces);
                    Log.GuardaLog(0, 0, _pay.messageTicket, int.Parse(_pay.code), "pay", identificadorServidor);
                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(stringwriter.ToString().Replace("pay", "OLS").Replace("xmlns:", "").Replace("utf", "UTF"), Encoding.UTF8, "application/xml")
                    };
                }

                string tranDate = Log.ObtieneValorPorNombre(xmlDocument, "tranDate");//root.ChildNodes[2].InnerText;
                string cashMachine = Log.ObtieneValorPorNombre(xmlDocument, "cashMachine"); //root.ChildNodes[3].InnerText;
                string entryMode = Log.ObtieneValorPorNombre(xmlDocument, "entryMode"); //root.ChildNodes[4].InnerText;
                string ticket = Log.ObtieneValorPorNombre(xmlDocument, "ticket"); //root.ChildNodes[6].InnerText;
                string account = Log.ObtieneValorPorNombre(xmlDocument, "account"); //root.ChildNodes[7].InnerText;
                string amount = Log.ObtieneValorPorNombre(xmlDocument, "amount"); //root.ChildNodes[8].InnerText;
                string folio = Log.ObtieneValorPorNombre(xmlDocument, "folio"); //root.ChildNodes[9].InnerText;
                string adminDate = Log.ObtieneValorPorNombre(xmlDocument, "adminDate"); //root.ChildNodes[10].InnerText;
                string store = Log.ObtieneValorPorNombre(xmlDocument, "store"); //root.ChildNodes[11].InnerText;
                
                string[] arr = account.Split('-');
                //Vamos a checar que el client coincida con el account
                if (client.Length < 11 || client != identificadorServidor + Log.AgregaCerosIzquierda(arr[0], 5) + Log.AgregaCerosIzquierda(arr[1], 5))
                {
                    _pay.errorDesc = "INCONSISTENCIA DE PARAMETROS";
                    _pay.code = "22";
                    _pay.code = Log.AgregaCerosIzquierda(_pay.code, 2);
                    serializer.Serialize(stringwriter, _pay, namespaces);
                    Log.GuardaLog(0, 0, _pay.messageTicket, int.Parse(_pay.code), "pay", identificadorServidor);
                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(stringwriter.ToString().Replace("pay", "OLS").Replace("xmlns:", "").Replace("utf", "UTF"), Encoding.UTF8, "application/xml")
                    };
                }

                //Pasamos otra vez el cobra
                DBHelper db1 = new DBHelper(identificadorServidor);
                db1.agregarParametro("@CONTRATO", SqlDbType.Int, arr[0]);
                db1.agregarParametro("@CLV_SESSION", SqlDbType.Int, ParameterDirection.Output);
                db1.agregarParametro("@ERROR", SqlDbType.Int, ParameterDirection.Output);
                db1.agregarParametro("@MSG", SqlDbType.VarChar, ParameterDirection.Output, 500);
                db1.agregarParametro("@idCompania", SqlDbType.Int, arr[1]);
                db1.consultaOutput("CobraFS");

                //Ahora pasamos los datos para guardarlo en facturas 
                DBHelper db = new DBHelper(identificadorServidor);
                db.agregarParametro("@tranDate", SqlDbType.VarChar, tranDate);
                db.agregarParametro("@cashMachine", SqlDbType.VarChar, cashMachine);
                db.agregarParametro("@entryMode", SqlDbType.VarChar, entryMode);
                db.agregarParametro("@ticket", SqlDbType.VarChar, ticket);
                db.agregarParametro("@account", SqlDbType.VarChar, account);
                db.agregarParametro("@amount", SqlDbType.VarChar, amount);
                db.agregarParametro("@folio", SqlDbType.VarChar, folio);
                db.agregarParametro("@store", SqlDbType.VarChar, store);
                db.agregarParametro("@Clv_Session", SqlDbType.Int, int.Parse(db1.diccionarioOutput["@CLV_SESSION"].ToString()));
                SqlDataReader reader = db.consultaReader("GuardaPagoOxxo");
                IEnumerable<pay> resultadodb2 = db.MapDataToEntityCollection<pay>(reader);
                _pay = resultadodb2.First();
                reader.Close();
                db.cierraConexion();

                _pay.code = Log.AgregaCerosIzquierda(_pay.code, 2);
                serializer.Serialize(stringwriter, _pay, namespaces);
                Log.GuardaLog(int.Parse(arr[0]), int.Parse(arr[1]), _pay.messageTicket, int.Parse(_pay.code), "pay", identificadorServidor);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(stringwriter.ToString().Replace("pay", "OLS").Replace("xmlns:", "").Replace("utf", "UTF"), Encoding.UTF8, "application/xml")
                };
                
            }
            catch (Exception ex)
            {
                XmlSerializerNamespaces namespaces2 = new XmlSerializerNamespaces();
                namespaces2.Add("version", "1.0");
                pay _pay2 = new pay();
                _pay2.code = "07";
                _pay2.errorDesc = "SERVICIO NO DISPONIBLE";
                Log.GuardaLog(0, 0, ex.ToString(), int.Parse(_pay2.code), "pay", identificadorServidor);
                _pay2.code = Log.AgregaCerosIzquierda(_pay2.code, 2);
                StringWriter stringwriter2 = new Utf8StringWriter();
                var serializer2 = new XmlSerializer(_pay2.GetType());
                serializer2.Serialize(stringwriter2, _pay2, namespaces2);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(stringwriter2.ToString().Replace("pay", "OLS").Replace("xmlns:", "").Replace("utf", "UTF"), Encoding.UTF8, "application/xml")
                };
            }
        }

    }
}