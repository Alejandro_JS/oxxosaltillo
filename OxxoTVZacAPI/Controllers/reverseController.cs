﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml;
using System.Data;
using OxxoTVZacAPI.Models;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Text;
using System.Xml.Serialization;
using System.IO;

namespace OxxoTVZacAPI.Controllers
{
    public class reverseController : ApiController
    {
        // GET api/<controller>
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        // GET api/<controller>/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        // POST api/<controller>
        public HttpResponseMessage Post(HttpRequestMessage request)
        {
            XmlSerializerNamespaces namespaces = new XmlSerializerNamespaces();
            //namespaces.Add(string.Empty, string.Empty);
            namespaces.Add("version", "1.0");

            reverse _reverse = new reverse();
            StringWriter stringwriter = new Utf8StringWriter();
            var serializer = new XmlSerializer(_reverse.GetType());

            try
            {
                var content = request.Content;
                string xmlContent = content.ReadAsStringAsync().Result;
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.LoadXml(xmlContent);

                XmlNode root = xmlDocument.ChildNodes[1];
                string token = root.ChildNodes[0].InnerText;
                if (!Log.ValidaToken(token))
                {
                    _reverse.errorDesc = "INCONSISTENCIA DE PARAMETROS";
                    _reverse.code = "22";
                    _reverse.code = Log.AgregaCerosIzquierda(_reverse.code, 2);
                    serializer.Serialize(stringwriter, _reverse, namespaces);
                    //No sabemos donde guardar las reversas, así que lo comentamos aquí
                    //Log.GuardaLog(0, 0, _reverse.messageTicket, int.Parse(_reverse.code), "reverse");
                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(stringwriter.ToString().Replace("reverse", "OLS").Replace("xmlns:", "").Replace("utf", "UTF"), Encoding.UTF8, "application/xml")
                    };
                }
                string folio = root.ChildNodes[1].InnerText;
                string auth = root.ChildNodes[2].InnerText;

                List<string> identificadoresServidor = new List<string>();
                //identificadoresServidor.Add("1");
                //identificadoresServidor.Add("2");
                //identificadoresServidor.Add("3");
                //identificadoresServidor.Add("4");
                identificadoresServidor.Add("5");
                string identificadorServidor = "";
                foreach (string id in identificadoresServidor)
                {
                    DBHelper dbAux = new DBHelper(id);
                    dbAux.agregarParametro("@folio", SqlDbType.VarChar, folio);
                    dbAux.agregarParametro("@existe", SqlDbType.Int, ParameterDirection.Output);
                    dbAux.consultaOutput("ValidaFolioOxxo");
                    if (dbAux.diccionarioOutput["@existe"].ToString() == "1")
                    {
                        identificadorServidor = id;
                    }
                }

                DBHelper db = new DBHelper(identificadorServidor);
                db.agregarParametro("@folio", SqlDbType.VarChar, folio);
                db.agregarParametro("@auth", SqlDbType.VarChar, auth);
                SqlDataReader reader = db.consultaReader("CancelaPagoOxxo");
                IEnumerable<reverse> resultadodb2 = db.MapDataToEntityCollection<reverse>(reader);
                _reverse = resultadodb2.First();
                reader.Close();
                db.cierraConexion();

                _reverse.code = Log.AgregaCerosIzquierda(_reverse.code, 2);
                serializer.Serialize(stringwriter, _reverse, namespaces);
                if (_reverse.account == "")
                {
                    Log.GuardaLog(0, 0, _reverse.messageTicket, int.Parse(_reverse.code), "reverse", identificadorServidor);
                }
                else
                {
                    Log.GuardaLog(_reverse.account, _reverse.messageTicket, int.Parse(_reverse.code), "reverse", identificadorServidor);
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(stringwriter.ToString().Replace("reverse", "OLS").Replace("xmlns:", "").Replace("utf", "UTF"), Encoding.UTF8, "application/xml")
                };
            }
            catch (Exception ex)
            {
                _reverse = new reverse();
                _reverse.code = "07";
                _reverse.errorDesc = "SERVICIO NO DISPONIBLE";
                //Log.GuardaLog(0, 0, ex.ToString(), int.Parse(_reverse.code), "reverse");
                _reverse.code = Log.AgregaCerosIzquierda(_reverse.code, 2);
                StringWriter stringwriter2 = new Utf8StringWriter();
                var serializer2 = new XmlSerializer(_reverse.GetType());
                serializer2.Serialize(stringwriter2, _reverse, namespaces);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(stringwriter2.ToString().Replace("reverse", "OLS").Replace("xmlns:", "").Replace("utf", "UTF"), Encoding.UTF8, "application/xml")
                };
            }
        }

        // PUT api/<controller>/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        // DELETE api/<controller>/5
        //public void Delete(int id)
        //{
        //}
    }
}